while True:
    year = input("Enter a year: ")
    if not year.isnumeric():
        print("Please enter a valid year.")
        continue
    year = int(year)
    if year <= 0:
        print("Year should be a positive non-zero value.")
        continue
    break

if (year % 4 == 0) and (year % 100 != 0) or (year % 400 == 0):
    print(year, "is a leap year")
else:
    print(year, "is not a leap year")
    
rows = int(input("Enter the number of rows: "))
cols = int(input("Enter the number of columns: "))

for i in range(rows):
    for j in range(cols):
        print("*", end="")
    print()